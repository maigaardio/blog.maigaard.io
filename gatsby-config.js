module.exports = {
  siteMetadata: {
    title: `Viktors Blog`
  },
  plugins: [
      `gatsby-plugin-react-helmet`,
      {
          resolve: 'gatsby-source-wordpress',
          options: {
              /*
             * The base URL of the Wordpress site without the trailingslash and the protocol. This is required.
             * Example : 'gatsbyjsexamplewordpress.wordpress.com' or 'www.example-site.com'
             */
              baseUrl: 'maigaardblog.wordpress.com',
              // The protocol. This can be http or https.
              protocol: 'http',
              // Indicates whether the site is hosted on wordpress.com.
              // If false, then the asumption is made that the site is self hosted.
              // If true, then the plugin will source its content on wordpress.com using the JSON REST API V2.
              // If your site is hosted on wordpress.org, then set this to false.
              hostingWPCOM: true,
              // If useACF is true, then the source plugin will try to import the Wordpress ACF Plugin contents.
              // This feature is untested for sites hosted on Wordpress.com.
              // Defaults to true.
              useACF: false,
              auth: {
                  // If auth.user and auth.pass are filled, then the source plugin will be allowed
                  // to access endpoints that are protected with .htaccess.
                  htaccess_user: 'your-htaccess-username',
                  htaccess_pass: 'your-htaccess-password',
                  htaccess_sendImmediately: false,

                  // If hostingWPCOM is true then you will need to communicate with wordpress.com API
                  // in order to do that you need to create an app (of type Web) at https://developer.wordpress.com/apps/
                  // then add your clientId, clientSecret, username, and password here
                  wpcom_app_clientSecret: 'xCljSHpLs8w9zbmPTlYdfZ0aIccVCqee6DhjIwfC45XNuvcWR4vojBxckxfGPkTL',
                  wpcom_app_clientId: '56034',
                  wpcom_user: 'viktor@maigaard.io',
                  wpcom_pass: 'Karla2077@'
              },
              // Set verboseOutput to true to display a verbose output on `npm run develop` or `npm run build`
              // It can help you debug specific API Endpoints problems
              verboseOutput: false
          }
      },
      {
          resolve: `gatsby-plugin-google-analytics`,
          options: {
              trackingId: 'UA-109694194-1',
              // Setting this parameter is optional
              anonymize: true
          },
      },
  ]
};
