/**
 *
 */
const AWS = require('aws-sdk'),
    fs = require('fs'),
    mime = require('mime-types');

args = require('./argparser');

console.log(args);

const s3 = new AWS.S3();

s3.listBuckets(function (err, data) {
    console.log(err, data);
});

const filename = args.file;

uploadToBucket(filename);

function uploadToBucket(f){
    fs.stat(f, function (err, stat) {
        if(stat.isDirectory()){
            const files = fs.readdirSync(f);
            files.forEach(function (file) {
                uploadToBucket(f + '/' + file);
            })
        }else{
            const remote_path = f.replace(args.file + '/', '');
            s3.putObject({
                Bucket: "aws-website-blogmaigaardio-d4q49",
                Key: remote_path,
                Body: fs.readFileSync('./' + f),
                ACL: "public-read",
                ContentType: mime.lookup('./' + f)
            }, function(err, data) {
                if (err) console.log(err, err.stack); // an error occurred
                else     console.log(data);           // successful response
            });
        }
    });
}
