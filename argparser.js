/**
 *
 * @type {ArgumentParser}
 */
var ArgumentParser = require("argparse").ArgumentParser;
var EventEmitter = require('events').EventEmitter;

module.exports = new EventEmitter();

var parser = new ArgumentParser({
    version: '0.0.1',
    addHelp:true,
    description: 'Recursive uploader for AWS Buckets'
});

parser.addArgument(
    [ '-f', '--file' ],
    {
        help: 'the file or directory you want to upload',
        required: true
    }
);
parser.addArgument(
    [ '-b', '--bucket' ],
    {
        help: 'The bucket name to upload to',
        defaultValue: "maigaard"
    }
);
parser.addArgument(
    [ '-R', '--recursive' ],
    {
        help: 'if you want to recursively upload subfolders as well',
        defaultValue: true
    }
);
args = parser.parseArgs();
module.exports = args;